# -*- coding: utf-8 -*-

import unittest
from JsonConverter import JsonConverter

class TestJsonConverter(unittest.TestCase):

    def test(self):
        data = [{"h3": "Title #1","div": "Hello, World 1!"},{"h3": "Title #2","div": "Hello, World 2!"}]
        json_converter = JsonConverter()
        output_data = json_converter.show(data)
        self.assertEqual(output_data, '<ul><li><h3>Title #1</h3><div>Hello, World 1!</div></li><li><h3>Title #2</h3><div>Hello, World 2!</div></li></ul>')


if __name__ == '__main__':
    unittest.main()