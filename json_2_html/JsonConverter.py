# -*- coding: utf-8 -*-

import re
import os
import sys
import json
from html import escape


PATH_INPUT_DATA = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    '..',
    'source.json'
)


class JsonConverter(object):
    tag_regex = re.compile(r'^[^#\.]+')
    tag_id_regex = re.compile(r'\#([^#\.]+)')
    tag_class_regex = re.compile(r'\.([^#\.]+)')

    def load(self, path=PATH_INPUT_DATA):
        return json.load(open(path))

    def show(self, input_data):
        return self.showList(input_data) if isinstance(input_data, list) else self.showPart(input_data)

    def showPart(self, part):
        output_items = []
        for tag, content in part.items():
            content_item = self.showList(content) if isinstance(content, list) else escape(content)
            start_tag = self.transformStartTag(tag)
            end_tag = self.tag_regex.findall(tag)[0]
            output_items.append('<{0}>{1}</{2}>'.format(start_tag, content_item, end_tag))

        return ''.join(output_items)

    def showList(self, data):
        list_items = ''.join(map(self.showListPart, data))
        return ''.join(['<ul>', list_items, '</ul>'])

    def showListPart(self, part):
        return '<li>{0}</li>'.format(self.showPart(part))

    def transformStartTag(self, tag_string):
        tag = self.tag_regex.findall(tag_string)[0]
        ids = self.tag_id_regex.findall(tag_string)
        ids = ' id="{0}"'.format(' '.join(ids)) if ids else ''
        classes = self.tag_class_regex.findall(tag_string)
        classes = ' class="{0}"'.format(' '.join(classes)) if classes else ''

        return tag + ids + classes

if __name__ == '__main__':
    json_converter = JsonConverter()
    input_data = json_converter.load(PATH_INPUT_DATA)
    sys.stdout.write(json_converter.show(input_data))

